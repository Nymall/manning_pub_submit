class Price:
    def __init__(self, part_number, price):
            self.price = price
            self.part_number = part_number

    def get_price(self):
        return self.price

def set_discount(self, percent_off): #removed get_price from the list of arguments - as we do not have an array, there is nothing to sort through
    self.percent_off = (percent_off * 0.01)

def get_discount_price(self):
    return float(self.price) - (float(self.price) * self.percent_off)

Price.set_discount = set_discount
Price.get_discount_price = get_discount_price

newprice = Price("22555", 25)

newprice.set_discount(50)
print(newprice.get_discount_price())
