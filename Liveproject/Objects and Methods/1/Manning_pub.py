class Price:
    def __init__(self, part_number, price):
            self.price = price
            self.part_number = part_number

    def get_price(self):
        return self.price

def function__init__(self, part_number, price):
    self.price = price
    self.part_number = part_number

def function_get_price(self):
    return self.price

namespace = {"__init__": function__init__, "get_price": function_get_price}

Price2 = type('price2', (), namespace)

newprice1 = Price(1,5)
newprice2 = Price2(2,6)

print(newprice1.get_price())
print(newprice1)
print(newprice2.get_price())
print(newprice2)
