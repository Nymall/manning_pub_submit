a = 1
b = a
b = 2
print(a, b)

c = "alabaster"
d = c
d = "conjak"
print(c, d)

e = [1, 2, 3]
f = e
f = [9, 6, 3]
print(e, f)

g = ["horse", "cart", "llama"]
h = g
h = ["pig", "dog", "buffalo"]
print(g, h)
h[1] = "motorcar"
print(g, h)

i = 34.56
j = i
j = 55.22
print(i, j)

k = {
    "color": "pink",
    "flavor": "cheese",
    "amount": "lots"
    }
l = k
l = {
    "color": "green",
    "flavor": "yogurt",
    "amount": "many"
    }
print(k, l)

m = {"berry", "Yogurt", "yurt"}
n = m
n = {"cheese", "cake", "olive"}
print(m, n)
n = {"chs", "ck", "lv"}
print(m, n)


