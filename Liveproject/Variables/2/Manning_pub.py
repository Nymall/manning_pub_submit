def vartest(vara, varb, change = 0, newval = 0):
    a = vara
    b = a
    print("A and B before tests:", a, b)
    if(change != 0):
        b[change] = newval
        print("A and B after item ", change, "change in B:", a, b)
    b = varb
    print("A and B after tests:", a, b)

vartest(1,2)#Variable : Can be overriden, does not override first var
vartest("potato", "turnip") #String: Can be overriden, does not override first var
e = [1, 2, 3]
f = [9, 6, 3]
vartest(e, f, 1, 9) #list: any changes in b affect a, but can be overriden

g = ["horse", "cart", "llama"]
h = ["pig", "dog", "buffalo"]
vartest(g, h, 1, "avalon")#list(string): any changes in b affect a, but can be overriden

vartest(1.5,2.37)#FLoat: Can be overriden, does not override first var

k = {
    "color": "pink",
    "flavor": "cheese",
    "amount": "lots"
    }
l = {
    "color": "green",
    "flavor": "yogurt",
    "amount": "many"
    }
vartest(k,l, "color", "peach") #dictionary: any changes in b affect a, but can be overriden

m = {"berry", "Yogurt", "yurt"}
n = {"cheese", "cake", "olive"}
vartest(g, h, 1, "avalon") #Set: Can be overriden, does not override first var


